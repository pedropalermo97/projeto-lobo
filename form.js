const $ = document
const url = "https://lobinhos.herokuapp.com/wolves/"

let form = $.querySelector("#formulario")
form.addEventListener("submit", e =>{
    e.preventDefault()
    let name = $.querySelector("#name").value
    let age = $.querySelector("#age").value
    let link_image = $.querySelector("#link_image").value
    let description = $.querySelector("#description").value

    let fetchBody = {
        "wolf": {
            "name": name,
            "age": age,
            "link_image": link_image,
            "status": "To Adopt",
            "description": description
        }
    }
    let fetchConfig = {
        method: "POST",
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(fetchBody)
    }
    fetch(url, fetchConfig)
    .then(resp => resp.json())
    .then(resp => console.log(resp))
})

window.onscroll = function () { myFunction() };
function myFunction() {
    if (document.documentElement.scrollTop > 400) {
        $.querySelector("header").classList.add("menor")
    } else {
        $.querySelector("header").classList.remove("menor")
    }
}

function redirecionar() {
    window.location.href = "quemSomos.html"
}

function redirecionar2() {
    window.location.href = "adicionarLobo.html"
}
